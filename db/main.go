package main

import (
	"log"
	"path/filepath"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/plugins/migratecmd"
)

func main() {
	var app = pocketbase.New()

	// enable creation of migration files on database schema change
	migratecmd.MustRegister(app, app.RootCmd, &migratecmd.Options{
		Automigrate: true,
		Dir:         filepath.Join(app.DataDir(), "../pb_migrations"),
	})

	// run pocketbase otherwise unchanged
	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}
