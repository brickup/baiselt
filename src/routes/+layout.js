// Documentation: https://kit.svelte.dev/docs/page-options#prerender
export const prerender = true;

// Documentation: https://kit.svelte.dev/docs/page-options#ssr
export const ssr = true;

// Documentation: https://kit.svelte.dev/docs/page-options#trailingslash
export const trailingSlash = 'ignore';
