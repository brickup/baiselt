import PocketBase from 'pocketbase';

const pb = new PocketBase(import.meta.env.MY_POCKETBASE_URL);

export default pb;
