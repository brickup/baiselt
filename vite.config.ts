import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [sveltekit()],
	envPrefix: 'MY',
	resolve: {
		alias: {
			'@ui': __dirname + '/src'
		}
	}
});
